import numpy as np
#from lightfm.datasets import fetch_movielens
from lightfm import LightFM
from numpy import array
import pandas as pd
from movielens import fetch_movielens

#CHALLENGE part 1 of 3 - write your own fetch and format method for a different recommendation
#dataset. Here a good few https://gist.github.com/entaroadun/1653794 
#And take a look at the fetch_movielens method to see what it's doing 
#

#fetch data and format it
data = fetch_movielens(min_rating=4.0)

#print training and testing data
#print(repr(data['train']))
#print(repr(data['test']))


#CHALLENGE part 2 of 3 - use 3 different loss functions (so 3 different models), compare results, print results for
#the best one. - Available loss functions are warp, logistic, bpr, and warp-kos.

#create model
model = LightFM(loss='warp')
#train model
model.fit(data['train'], epochs=30, num_threads=2)


#CHALLENGE part 3 of 3 - Modify this function so that it parses your dataset correctly to retrieve
#the necessary variables (products, songs, tv shows, etc.)
#then print out the recommended results 

def sample_recommendation(model, data, user_ids):

    #number of users and movies in training data
    n_users, n_items = data['train'].shape
    #944 users and 1682 items
    # first user id is 0, last user id is 943


    #generate recommendations for each user we input
    for user_id in user_ids:

        #movies they already like
        known_positives = data['item_labels'][data['train'].tocsr()[user_id].indices] #select the indices that maps the user_id and then find the corresponding movies

        #movies our model predicts they will like
        scores = model.predict(user_id, np.arange(n_items)) #np.arrange(1682)= array([0,1,2,3,......1681]) (with np arrays we can make calculations)
        #scores is an array: [-0.13669443 -1.86131454 -1.31664503 ..., -2.08644581 -2.67313123 -2.70647359]

        #rank them in order of most liked to least
        top_items = data['item_labels'][np.argsort(-scores)] #example: np.argsort(np.array([3, 1, 2])) -> [1 2 0]


        print("-------------")
        #print(np.sort(scores)[::-1]) #scores descending order (array with scores and size 1682)
        #print(np.argsort(-scores)) #indices in descending order (array with indices and size 1682)
        indMat = np.sort(scores)[::-1]
        s = pd.Series(np.argsort(-scores), index=indMat)

        print(s)
        print("-------------")



        #top_items is an array (length=1682):
        #['Contact (1997)' 'Air Force One (1997)' 'Scream (1996)' ...,
        #'American in Paris, An (1951)' 'Candidate, The (1972)'
        #'Cat on a Hot Tin Roof (1958)']

        #from the top items, remove the known positives
        final_top_items = []

        for top in top_items:
            i = 0;
            for positive in known_positives:
                if(top == positive):
                    i = 1;
            if(i == 0):
                final_top_items.append(top)


        #print out the results
        print("User %s" % user_id)
        print("     Known positives:")

        for x in known_positives[:20]:
            print("        %s" % x)

        print("     Recommended:")

        for x in final_top_items[:10]:
            print("        %s" % x)
            
sample_recommendation(model, data, [943])
