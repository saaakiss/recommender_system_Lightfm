import itertools
import zipfile

import numpy as np

import scipy.sparse as sp

from lightfm.datasets import _common


def _read_raw_data(path):
    """
    Return the raw lines of the train and test files.
    """

    with zipfile.ZipFile(path) as datafile: #instead of u.data the default was ua.base
        return (datafile.read('ml-100k/u.data').decode().split('\n'),
                datafile.read('ml-100k/ua.test').decode().split('\n'),
                datafile.read('ml-100k/u.item').decode(errors='ignore').split('\n'),
                datafile.read('ml-100k/u.genre').decode(errors='ignore').split('\n'))


def _parse(data):

    for line in data:

        if not line:
            continue

        uid, iid, rating, timestamp = [int(x) for x in line.split('\t')]

        # Subtract one from ids to shift
        # to zero-based indexing
        yield uid - 1, iid - 1, rating, timestamp


def _get_dimensions(train_data):

    uids = set()
    iids = set()

    for uid, iid, _, _ in itertools.chain(train_data):
        uids.add(uid)
        iids.add(iid)


    rows = max(uids) + 1
    cols = max(iids) + 1

    return rows, cols


def _build_interaction_matrix(rows, cols, data, min_rating):

    mat = sp.lil_matrix((rows, cols), dtype=np.int32) #943x1682 sparse matrix of type np.int32
    #mat.todense():
    #[[0 0 0 ..., 0 0 0]
    #[0 0 0 ..., 0 0 0]
    #[0 0 0 ..., 0 0 0]
    #...,
    #[0 0 0 ..., 0 0 0]
    #[0 0 0 ..., 0 0 0]
    #[0 0 0 ..., 0 0 0]]



    for uid, iid, rating, _ in data:
        #in the sparse matrix (mat) store only the data that satisfy the condition
        if rating >= min_rating:
            mat[uid, iid] = rating


    #e.g. mat[0,0] = 5 (uid=0, iid=0)
    #mat.todense():
    #[[5 3 4 ..., 0 0 0]
    #[4 0 0 ..., 0 0 0]
    #[0 0 0 ..., 0 0 0]
    #...,
    #[5 0 0 ..., 0 0 0]
    #[0 0 0 ..., 0 0 0]
    #[0 5 0 ..., 0 0 0]]


    return mat.tocoo()


def _parse_item_metadata(num_items,
                         item_metadata_raw, genres_raw):

    genres = []

    for line in genres_raw:
        if line:
            genre, gid = line.split('|')
            genres.append('genre:{}'.format(genre))

    id_feature_labels = np.empty(num_items, dtype=np.object)
    genre_feature_labels = np.array(genres)

    id_features = sp.identity(num_items,
                              format='csr',
                              dtype=np.float32)
    genre_features = sp.lil_matrix((num_items, len(genres)),
                                   dtype=np.float32)

    for line in item_metadata_raw:

        if not line:
            continue

        splt = line.split('|')

        # Zero-based indexing
        iid = int(splt[0]) - 1
        title = splt[1]

        id_feature_labels[iid] = title

        item_genres = [idx for idx, val in
                       enumerate(splt[5:])
                       if int(val) > 0]

        for gid in item_genres:
            genre_features[iid, gid] = 1.0

    return (id_features, id_feature_labels,
            genre_features.tocsr(), genre_feature_labels)


def fetch_movielens(data_home=None, indicator_features=True, genre_features=False,
                    min_rating=0.0, download_if_missing=True):
    """
    Fetch the `Movielens 100k dataset <http://grouplens.org/datasets/movielens/100k/>`_.

    The dataset contains 100,000 interactions from 1000 users on 1700 movies,
    and is exhaustively described in its
    `README <http://files.grouplens.org/datasets/movielens/ml-100k-README.txt>`_.

    Parameters
    ----------

    data_home: path, optional
        Path to the directory in which the downloaded data should be placed.
        Defaults to ``~/lightfm_data/``.
    indicator_features: bool, optional
        Use an [n_users, n_users] identity matrix for item features. When True with genre_features,
        indicator and genre features are concatenated into a single feature matrix of shape
        [n_users, n_users + n_genres].
    genre_features: bool, optional
        Use a [n_users, n_genres] matrix for item features. When True with item_indicator_features,
        indicator and genre features are concatenated into a single feature matrix of shape
        [n_users, n_users + n_genres].
    min_rating: float, optional
        Minimum rating to include in the interaction matrix.
    download_if_missing: bool, optional
        Download the data if not present. Raises an IOError if False and data is missing.

    Notes
    -----

    The return value is a dictionary containing the following keys:

    Returns
    -------

    train: sp.coo_matrix of shape [n_users, n_items]
         Contains training set interactions.
    test: sp.coo_matrix of shape [n_users, n_items]
         Contains testing set interactions.
    item_features: sp.csr_matrix of shape [n_items, n_item_features]
         Contains item features.
    item_feature_labels: np.array of strings of shape [n_item_features,]
         Labels of item features.
    item_labels: np.array of strings of shape [n_items,]
         Items' titles.
    """

    if not (indicator_features or genre_features):
        raise ValueError('At least one of item_indicator_features '
                         'or genre_features must be True')

    zip_path = _common.get_data(data_home,
                                ('https://github.com/maciejkula/'
                                 'lightfm_datasets/releases/'
                                 'download/v0.1.0/movielens.zip'),
                                'movielens100k',
                                'movielens.zip',
                                download_if_missing)


    #-------------------------------------------------------------------------------------------------------------------
    # Load raw data
    (train_raw, test_raw,
     item_metadata_raw, genres_raw) = _read_raw_data(zip_path)

    #train_raw, test_raw:
    #[user id | item id | rating | timestamp]. For example: ['487\t684\t5\t883446543', '487\t685\t3\t883444252']

    # item_metadata:
    # movie id | movie title | release date | video release date |
    #          IMDb URL | unknown | Action | Adventure | Animation |
    #         Children's | Comedy | Crime | Documentary | Drama | Fantasy |
    #        Film-Noir | Horror | Musical | Mystery | Romance | Sci-Fi |
    #       Thriller | War | Western |
    #      The last 19 fields are the genres, a 1 indicates the movie
    #     is of that genre, a 0 indicates it is not; movies can be in
    #    several genres at once.
    # For example:
    #   ['1|Toy Story (1995)|01-Jan-1995||http://us.imdb.com/M/title-exact?Toy%20Story%20(1995)|0|0|0|1|1|1|0|0|0|0|0|0|0|0|0|0|0|0|0', ....]

    # genres_raw:
    # ['unknown|0', 'Action|1', 'Adventure|2', 'Animation|3', "Children's|4", 'Comedy|5',
    # 'Crime|6', 'Documentary|7', 'Drama|8', 'Fantasy|9', 'Film-Noir|10', 'Horror|11', 'Musical|12', 'Mystery|13',
    # 'Romance|14', 'Sci-Fi|15', 'Thriller|16', 'War|17', 'Western|18', '', '']
    #-------------------------------------------------------------------------------------------------------------------


    #-------------------------------------------------------------------------------------------------------------------
    # Figure out the dimensions without the new user
    num_users, num_items = _get_dimensions(_parse(train_raw))

    # num_users = 943
    # num_items = 1682
    #-------------------------------------------------------------------------------------------------------------------


    #-------------------------------------------------------------------------------------------------------------------
    # Load metadata features
    (id_features, id_feature_labels,
     genre_features_matrix, genre_feature_labels) = _parse_item_metadata(num_items,
                                                                         item_metadata_raw,
                                                                         genres_raw)
    # id_features:
    # <1682x1682 sparse matrix of type '<class 'numpy.float32'>'
    # with 1682 stored elements in Compressed Sparse Row format>
    # (0, 0)	1.0
    # (1, 1)	1.0
    # (2, 2)	1.0
    # (3, 3)	1.0
    # (4, 4)	1.0
    # (5, 5)	1.0 .......

    # id_feature_labels (title) is an array of:
    # ['Toy Story (1995)' 'GoldenEye (1995)' 'Four Rooms (1995)' ...,
    # 'Sliding Doors (1998)' 'You So Crazy (1994)'
    # 'Scream of Stone (Schrei aus Stein) (1991)']

    # genre_features_matrix <1682x19 sparse matrix of type '<class 'numpy.float32'>'
    # with 2893 stored elements in Compressed Sparse Row format>
    # (num_items, len(genres))
    # (0, 3)	1.0
    # (0, 4)	1.0
    # (0, 5)	1.0
    # (1, 1)	1.0

    # genre_feature_labels:
    # ['genre:unknown' 'genre:Action' 'genre:Adventure' 'genre:Animation'
    # "genre:Children's" 'genre:Comedy' 'genre:Crime' 'genre:Documentary'
    # 'genre:Drama' 'genre:Fantasy' 'genre:Film-Noir' 'genre:Horror'
    # 'genre:Musical' 'genre:Mystery' 'genre:Romance' 'genre:Sci-Fi'
    # 'genre:Thriller' 'genre:War' 'genre:Western']
    #-------------------------------------------------------------------------------------------------------------------


    #-------------------------------------------------------------------------------------------------------------------
    # inserting new user
    ''' manualy enter random ratings for 20 random movies for the new user (only for testing) 
        train_raw.append('944\t684\t5\t883446543')
        train_raw.append('944\t685\t5\t883446543')
        train_raw.append('944\t686\t5\t883446543')
        train_raw.append('944\t687\t5\t883446543')
        train_raw.append('944\t688\t5\t883446543')
        train_raw.append('944\t689\t5\t883446543')
        train_raw.append('944\t690\t5\t883446543')
        train_raw.append('944\t691\t5\t883446543')
        train_raw.append('944\t692\t5\t883446543')
        train_raw.append('944\t693\t5\t883446543')
        train_raw.append('944\t694\t5\t883446543')
        train_raw.append('944\t695\t5\t883446543')
        train_raw.append('944\t696\t5\t883446543')
        train_raw.append('944\t697\t5\t883446543')
        train_raw.append('944\t698\t5\t883446543')
        train_raw.append('944\t699\t5\t883446543')
        train_raw.append('944\t700\t5\t883446543')
        train_raw.append('944\t701\t5\t883446543')
        train_raw.append('944\t702\t5\t883446543')
        train_raw.append('944\t703\t5\t883446543')
    '''

    #insert new user by creating gui in python console
    newUserID = 944  # new user's id
    timestamp = 883446543  # random timestamp, we dont care about that

    i=0
    j=20
    f=0
    while(f<5):

        userList = []

        for x in range(i,j):
            userList.append({x%20+1: id_feature_labels[x]})

        print('\n')

        for p in userList:
            print(p)

        print('\n')
        ui = input("Choose a movie, or press -1 to change movieset: ")
        var = int(ui)

        if (var == -1):
            if ((1681 - j) >= 20):
                i = j
                j += 20
            elif ((1681 - j) > 0):
                i = j
                j = 1682
            else:
                i = 0
                j = 20
            continue
        else:
            print('\n')
            print("You selected the movie: " + userList[var-1][var] + " with ID: " + str(id_feature_labels.tolist().index(userList[var-1][var])+1))
            print('\n')
            rating = input("Rate the movie: ")
            train_raw.append('944\t' + str(id_feature_labels.tolist().index(userList[var-1][var])+1) + '\t' + rating + '\t883446543')
            f = f + 1

            while(f < 5):
                ch = input("To change the movieset press -1, to keep press 1: ")
                if(int(ch) == -1):
                    break
                else:
                    print('\n')
                    for p in userList:
                        print(p)
                    print('\n')
                    mov = input("Choose a movie: ")
                    var = int(mov)
                    print('\n')
                    print("You selected the movie: " + userList[var - 1][var] + " with ID: " + str(
                        id_feature_labels.tolist().index(userList[var - 1][var]) + 1))
                    print('\n')
                    rating = input("Rate the movie: ")
                    train_raw.append('944\t' + str(
                        id_feature_labels.tolist().index(userList[var - 1][var]) + 1) + '\t' + rating + '\t883446543')
                    f = f + 1

            if((1681-j) >= 20):
                i = j
                j += 20
            elif((1681-j) > 0):
                i = j
                j = 1682
            else:
                i = 0
                j = 20

        print('\n')




    #len(train_raw) = 90591
    #first user id is 1, last user id is 944
    #-------------------------------------------------------------------------------------------------------------------


    #-------------------------------------------------------------------------------------------------------------------
    # Figure out the dimensions with the new user
    num_users, num_items = _get_dimensions(_parse(train_raw))

    # num_users = 944
    # num_items = 1682
    #-------------------------------------------------------------------------------------------------------------------


    #-------------------------------------------------------------------------------------------------------------------
    # Load train interactions
    train = _build_interaction_matrix(num_users,
                                      num_items,
                                      _parse(train_raw),
                                      min_rating)

    #train matrix:
    #(0, 0)	5
    #(0, 2)	4
    #(0, 5)	5
    #(0, 6)	4
    #(0, 8)	5
    #first user id is 0, last user id is 943
    #-------------------------------------------------------------------------------------------------------------------


    #-------------------------------------------------------------------------------------------------------------------
    # Load test interactions
    #test = _build_interaction_matrix(num_users,num_items,_parse(test_raw),min_rating)

    # test matrix:
    # (0, 0)	5
    # (0, 2)	4
    # (0, 5)	5
    # (0, 6)	4
    # (0, 8)	5

    test = []

    # assert train.shape == test.shape
    # (943, 1682) == (943, 1682)
    #-------------------------------------------------------------------------------------------------------------------


    #-------------------------------------------------------------------------------------------------------------------
    '''# Load metadata features (we already defined it above!)
    (id_features, id_feature_labels,
     genre_features_matrix, genre_feature_labels) = _parse_item_metadata(num_items,
                                                                         item_metadata_raw,
                                                                         genres_raw)
    #id_features:
    #<1682x1682 sparse matrix of type '<class 'numpy.float32'>'
	#with 1682 stored elements in Compressed Sparse Row format>
    #(0, 0)	1.0
    #(1, 1)	1.0
    #(2, 2)	1.0
    #(3, 3)	1.0
    #(4, 4)	1.0
    #(5, 5)	1.0 .......


    #id_feature_labels (title) is an array of:
    #['Toy Story (1995)' 'GoldenEye (1995)' 'Four Rooms (1995)' ...,
    #'Sliding Doors (1998)' 'You So Crazy (1994)'
    #'Scream of Stone (Schrei aus Stein) (1991)']

    #genre_features_matrix <1682x19 sparse matrix of type '<class 'numpy.float32'>'
	#with 2893 stored elements in Compressed Sparse Row format>
    #(num_items, len(genres))
    #(0, 3)	1.0
    #(0, 4)	1.0
    #(0, 5)	1.0
    #(1, 1)	1.0

    #genre_feature_labels:
    #['genre:unknown' 'genre:Action' 'genre:Adventure' 'genre:Animation'
    #"genre:Children's" 'genre:Comedy' 'genre:Crime' 'genre:Documentary'
    #'genre:Drama' 'genre:Fantasy' 'genre:Film-Noir' 'genre:Horror'
    #'genre:Musical' 'genre:Mystery' 'genre:Romance' 'genre:Sci-Fi'
    #'genre:Thriller' 'genre:War' 'genre:Western']
    '''
    #-------------------------------------------------------------------------------------------------------------------


    assert id_features.shape == (num_items, len(id_feature_labels)) #1682x1682 = 1682x1682
    assert genre_features_matrix.shape == (num_items, len(genre_feature_labels)) #1682x19 = 1682x19

    if indicator_features and not genre_features:
        features = id_features
        feature_labels = id_feature_labels
    elif genre_features and not indicator_features:
        features = genre_features_matrix
        feature_labels = genre_feature_labels
    else:
        features = sp.hstack([id_features, genre_features_matrix]).tocsr()
        feature_labels = np.concatenate((id_feature_labels,
                                         genre_feature_labels))

    data = {'train': train,
            'test': test,
            'item_features': features,
            'item_feature_labels': feature_labels,
            'item_labels': id_feature_labels}

    return data
