import pandas as pd
import sys, os
import numpy as np
from random import randint



pathname = os.path.dirname(sys.argv[0]) #C:/Users/Sak/PycharmProjects/recommender_system_challenge_Lightfm



directory = os.path.abspath(pathname) #C:\Users\Sak\PycharmProjects\recommender_system_challenge_Lightfm


if os.name == 'posix':
    descriptor = '/'
    def clear_screen():
        os.system('clear')
else:
    descriptor = '\\' #os name is 'nt', so we are inside else, the descriptor is '\', and also we define the function clear_screen
    def clear_screen():
        os.system('cls')


def get_user_movie_id():
    q = 'Put the movie ID of the movie you want to rate, or -1 to refresh list:'
    # Fix the '0{1,2,3,4,5,6,7,8,9}' bug
    while True:
        choice = input(q + '\n')
        if choice == '01' or \
            choice == '02' or \
            choice == '03' or \
            choice == '04' or \
            choice == '05' or \
            choice == '06' or \
            choice == '07' or \
            choice == '08' or \
            choice == '09':
                print ('Wrong Movie ID. Please do not use 0 prefix! Try again ...')

        else:
        # We are not affected from the bug
            try:
                choice = int(choice) # Try to convert string to integer
            except ValueError:
                # If it fails, ask the user again
                print ("Wrong Movie ID. This is not a number. Please try again ...")
                continue

            # If the conversion to integer has been succedded, check for its value
            if choice < -1 or choice > 1682 or choice == 0:
                # Not acceptable integer value
                print ('Wrong Movie ID. Please try again ...')
                continue
            # It's an acceptable integer value
            break # Break the loop
    return choice


def get_user_rating(TAINIA):
    movie = str(TAINIA)
    q = "How much would you rate '" + movie + "' ?"
    # Fix the '0{1,2,3,4,5,6,7,8,9}' bug
    while True:
        choice = input(q + '\n')
        if choice == '01' or \
            choice == '02' or \
            choice == '03' or \
            choice == '04' or \
            choice == '05' or \
            choice == '06' or \
            choice == '07' or \
            choice == '08' or \
            choice == '09':
                print ('Wrong Rating. Please do not use 0 prefix. Try again ...')
        else:
            # We are not affected from the bug
            try:
                choice = int(choice) # Try to convert string to integer
            except ValueError:
                # If it fails, ask the user again
                print ("Wrong Rating. This is not a number. Please try again ...")
                continue

            # If the conversion to integer has been succedded, check for its value
            if choice < 1 or choice > 5:
                # Not acceptable integer value
                print ('Wrong Rating. Please insert a value between [1-5]:')
                continue
                # It's an acceptable integer value
            break # Break the loop
    return choice


movieLensFile = directory + descriptor + 'u.data' #C:\Users\Sak\PycharmProjects\recommender_system_challenge_Lightfm\u.data
mostRatedFile = directory + descriptor + 'mostratedmovies.csv'


#Main

myUserDatabase = pd.read_csv(movieLensFile, sep="\t", header=None, names=['userId', 'itemId', 'rating'], usecols=[0, 1, 2])

'''
       userId  itemId  rating
0         196     242       3
1         186     302       3
2          22     377       1
3         244      51       2
4         166     346       1
5         298     474       4

[100000 rows x 3 columns]
'''


def newUser(ratingsmatrix):

    userID = 944
    print ('Welcome!\n')
    print ('We will present you a list of movies for rating. \nPlease enter the ID of the movie you want to rate and press Enter.\nThen put the corresponding rating (integer value in the range 1-5), with 1 being bad and 5 being excellent')
    print ('\n\nPress Enter to continue . . .')
    #useless = (input())
    print ('\n\n')
    myRatedDatabase = pd.read_csv(mostRatedFile, sep=",", header=None, names=['MOVIE_NAME', 'MOVIE_ID'], usecols=[0, 1])
    print ('\n\nMOVIE LIST\n\n')
    ii = -9
    jj = 1
    cnt = 1
    movlist = pd.DataFrame(index=range(0, 21, 1), columns=['MOVIE_NAME', 'MOVIE_ID'])
    while cnt <= 20:
        clear_screen()
        print ("MOVIE = " + str(cnt) + "/ 20")
        print ("-----------------------------------------------------")
        icount = 0
        jcount = 0
        ii += 10
        jj += 10
        movlist.loc[0].loc['MOVIE_NAME'] = ' '
        movlist.loc[0].loc['MOVIE_ID'] = ' '

        # Refresh the movie list with [ 21 x 2 ] -- print begins from row[1] and ends at row [20]
        # ------------------------------------------------------------
        for i in range(ii, jj):
             # row[1,3,5,7,9] are **not** random
             icount += 1
             movlist.loc[icount].loc['MOVIE_NAME'] = myRatedDatabase.iloc[i, 0]
             jcount += 1
             movlist.loc[icount].loc['MOVIE_ID'] = myRatedDatabase.iloc[i, 1]
             jcount -= 1

             # row[2,4,6,8,10] are random
             icount += 1
             ranc = randint(jj, 1682)
             movlist.loc[icount].loc['MOVIE_NAME'] = myRatedDatabase.iloc[ranc, 0]
             jcount += 1
             movlist.loc[icount].loc['MOVIE_ID'] = myRatedDatabase.iloc[ranc, 1]
             jcount -= 1

             # Print to screen
             print ('\t\t', movlist.to_string(index = False, header=False))




        # User input: Ask the user either to pick a Movie or refresh the list
        # -----------------------------------------------------------
        choice = get_user_movie_id()
        if choice == -1:
            # Redesign the movielist dataframe (+10 new movies)
            continue



        # Find the name of the movie based on the User input
        # ------------------------------------------------------------
        found = False
        for row in enumerate(movlist.values):
            MOVIE_INDEX = row[0]
            MOVIE_NAME = row[1][0]
            MOVIE_ID = row[1][1]
            if ( MOVIE_INDEX == 0 ):
                # The first row is always empty, please SKIP it
                continue
            if choice == int(MOVIE_ID):
                TAINIA = MOVIE_NAME
                found = True
                break
            elif MOVIE_INDEX >= 1 and MOVIE_INDEX <=20:
                # Search withing the 20 Listed movies atm
                continue

        # User selected a number that's not currently displays on top 20 list (cheat)
        if not found:
            for row2 in enumerate(myRatedDatabase.values):
                MOVIE_INDEX = row2[0]
                MOVIE_NAME = row2[1][0]
                MOVIE_ID = row2[1][1]
                if ( MOVIE_INDEX == 0 ):
                    continue
                if choice == int(MOVIE_ID):
                    TAINIA = MOVIE_NAME
                    found = True
                    break
                else:
                    continue

        if not found:
            print ("Internal error. I cannot find the movie with such ID.")
        exit(2)



        # User input: Ask the to rate the selected movie
        # -------------------------------------------------------

        rate = get_user_rating(TAINIA)
        print ('You rated ', TAINIA, ' with ', rate)




        # Record the user's opinion into the database (aka ratingmatrix)
        # ---------------------------------------------------------
        df = pd.DataFrame([[int(userID), int(choice), int(rate)]], columns=('userId', 'itemId', 'rating'))
        ratingsmatrix.loc[len(ratingsmatrix)] = df.loc[0]



        # Keep voting until 20 Movies
        # ----------------------------------------------------------
        cnt += 1

        print ('\n\nPlease Wait . . .\n\n')
        return ratingsmatrix


myUserDatabase = newUser(myUserDatabase)

