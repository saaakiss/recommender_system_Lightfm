from itertools import *
from sklearn.feature_extraction.text import TfidfVectorizer
import pandas as pd
import numpy as np

artists = {}
artists[50] = {
                        'name' : "sakis",
                        'id' : 10
                        }
artists[100] = {
                        'name' : "peter",
                        'id' : 11
                        }

uids = set()


corpus = [
    'This is the first document.',
    'This is the second second document.',
    'And the third one.',
    'Is this the first document?',
]

#the words are with alphabetical order

vectorizer = TfidfVectorizer()
mat = vectorizer.fit_transform(corpus)
idf = vectorizer.get_feature_names()

from scipy.sparse import coo_matrix, hstack
A = coo_matrix([[8,10],[50,100]])
B = coo_matrix([[5],[6]])
mat = hstack( [A,B] ).todense()



s = pd.Series(np.random.randn(5), index=['a', 'b', 'c', 'd', 'e'])

a = np.array([[10,1],[5,6]])
print(np.sort(a, axis=-1))
